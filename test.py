import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from subprocess import getoutput
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common import NoSuchElementException


# config
options = Options()
options.binary_location = getoutput("find /snap/firefox -name firefox").split("\n")[-1]

# time loading second
delay = int(5)

driver = webdriver.Firefox(service=Service(executable_path=getoutput("find /snap/firefox -name geckodriver").split(
    "\n")[-1]), options=options)
driver.get('https://support.bitchute.com/policy/terms')

try:
    terms_txt = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//h1[contains(text(), 'Terms & Conditions')]")))
    print(terms_txt.text)

except NoSuchElementException:
    print('element is not found')



# driver.implicitly_wait(delay)
'''
class AutoLoginFirefox(unittest.TestCase):
    def __init__(self):
        self.login = 'http://localhost:9000/login'
        self.register = 'http://localhost:9000/register'
        self.profile = 'http://localhost:9000/profile'

    def setUp(self):
        self.driver = webdriver.Firefox(
            service=Service(executable_path=getoutput("find /snap/firefox -name geckodriver").split(
                "\n")[-1]), options=options)

    def test_login(self):
        driver = self.driver
        driver.get(self.login)

        username = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.NAME, 'username')))
        password = driver.find_element(By.NAME, 'password')

        username.send_keys('jane')
        password.send_keys('1234', Keys.ENTER)
        time.sleep(5)

        # check path
        match driver.current_url:
            case self.login:
                change_path = driver.find_element(By.LINK_TEXT, 'Register')
                action = ActionChains(driver)
                action.move_to_element(change_path).click().perform()
                driver.implicitly_wait(delay)
                regis_username = driver.find_element(By.NAME, 'username')
                regis_email = driver.find_element(By.NAME, 'email')
                regis_password = driver.find_element(By.NAME, 'password')
                regis_submit = driver.find_element(By.NAME, 'submit')
                regis_username.send_keys('jane')
                regis_email.send_keys('jane@gmail.com')
                regis_password.send_keys('1234')
                action.move_to_element(regis_submit).click().perform()
                time.sleep(5)

                match driver.current_url:
                    case self.login:
                        driver.implicitly_wait(delay)
                        login_username = driver.find_element(By.NAME, 'username')
                        login_password = driver.find_element(By.NAME, 'password')
                        login_username.send_keys('jane')
                        login_password.send_keys('1234', Keys.ENTER)
                        time.sleep(5)

            case self.profile:
                driver.implicitly_wait(delay)
                profile_firstname = driver.find_element(By.NAME, 'firstname')
                profile_lastname = driver.find_element(By.NAME, 'lastname')
                profile_firstname.send_keys('Jennifer')
                profile_lastname.send_keys('Doe', Keys.ENTER)
                time.sleep(5)
                driver.refresh()
                # driver.quit()
                return


# element = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'search2')))

run = AutoLoginFirefox()
run.setUp()'''
# run.test_login()